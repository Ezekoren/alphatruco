import requests
import json
import time

data = {"saludo": "hello"}
URL = "http://localhost:5000/truco"

def make_post(data):
    json_data = json.dumps(data)
    r = requests.post(url=URL, data=json_data)
    response = json.loads(r.text)
    return response

def get_player():
    data = {"comm": "getPlayer"}
    response = make_post(data)
    player = response["response"]
    print("Sos el jugador ", player)
    if player == 1:
        while True:
            print("Esperando a otro jugador...")
            data = {"comm": "waitForPlayer"}
            response = make_post(data)
            if response["response"]:
                print("¡Jugador encontrado!")
                break
            time.sleep(2)
    elif player == 2:
        print("¡Jugador encontrado!")
    return player

def run():
    game_over = False
    player = get_player()
    if player == 1:
        data = {"comm": "startGame"}
        response = make_post(data)
    print("Partida Empezada")
    while not game_over:
        comm = input("Inerte lo que quiere hacer: ")
        if comm == "exit":
            game_over = True
        elif comm[:5] == "poner":
            data = {"comm": "drawCard", "player": "player"+str(player), "carta": comm[6:]}
            response = make_post(data)
            print(response["response"])
        else:
            data = {"comm": comm}
            response = make_post(data)
            

if __name__ == "__main__":
    run()