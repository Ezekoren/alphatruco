from flask import Flask, request
from flask_cors import CORS
import json
from truco import Game

app = Flask(__name__)
CORS(app)
game = Game()
player = 0

def manage_card(player, card):
    switch = {"player1": game.player1, "player2": game.player2}
    cartas_ronda = []
    if switch[player]["esTurno"]:
        cartas_ronda.append(switch[player]["mano"][int(card)])
        msg = "Pusiste el " + game.player1["mano"][int(card)]["nombre"]
    else:
        msg = "Lo siento, espera a tu turno"
    return cartas_ronda, msg

@app.route("/repartir", methods=["POST"])
def repartir():
    global player
    # req = json.loads(request.data)
    game.repartir()
    res = json.dumps({"response": "se repartió"})
    return res

@app.route("/getPlayer", methods=["POST"])
def getPlayer():
    global player
    # req = json.loads(request.data)
    if player <= 1:
        player += 1
        res = json.dumps({"response": player})
    else:
        res  = json.dumps({"response": -1})
    return res

@app.route("/startGame", methods=["POST"])
def startGame():
    global player
    # req = json.loads(request.data)        
    game.repartir()
    game.startMano()
    ## TODO: Hacer return

@app.route("/waitForPlayer", methods=["POST"])
def waitForPlayer():
    global player
    # req = json.loads(request.data)        
    isPlayer2 = True if player == 2 else False
    res = json.dumps({"response": isPlayer2})
    return res

@app.route("/drawCard", methods=["POST"])
def drawCard():
    global player
    req = json.loads(request.data) 
    cartas_ronda, msg = manage_card(req["player"], req["carta"])
    res = json.dumps({"response": msg})
    # TODO: Esta garcha
        
app.run()