import json
import random


class Mazo:
    def __init__(self):
        self.mazo = json.load(open("mazo.json", "r"))
        self.mezclar()

    def mezclar(self):
        random.shuffle(self.mazo)
        random.shuffle(self.mazo)
        random.shuffle(self.mazo)
        random.shuffle(self.mazo)
        random.shuffle(self.mazo)

    def cortar(self, carta):
        while carta > 0:
            self.mazo.append(self.mazo[0])
            self.mazo.remove(self.mazo[0])
            carta -= 1


class Game:
    def __init__(self):
        self.mazo = Mazo()
        self.newGame()

    def newGame(self):
        self.player1 = {
            "mano": [],
            "envido": 0,
            "puntos": 0,
            "esMano": True,
            "esTurno": True,
            "puedeCantar": True
        }
        self.player2 = {
            "mano": [],
            "envido": 0,
            "puntos": 0,
            "esMano": False,
            "esTurno": False,
            "puedeCantar": True
        }

    def startMano(self):
        self.won = False
        self.canEnvido = True
        self.puntosMano = 1
        self.rondas = []
        self.numRonda = 0
        self.added = False
        self.mesa = [[None, None], [None, None], [None, None]]
        self.repartir()

    def repartir(self):
        if len(self.player1["mano"]) > 0:
            self.finMano()
        self.mazo.mezclar()
        rand = random.randint(1, 40)
        print(rand)
        self.mazo.cortar(rand)
        if self.player1["esMano"] == True:
            v = 3
            while v > 0:
                self.player1["mano"].append(self.mazo.mazo[0])
                self.mazo.mazo.remove(self.mazo.mazo[0])
                self.player2["mano"].append(self.mazo.mazo[0])
                self.mazo.mazo.remove(self.mazo.mazo[0])
                v -= 1
        elif self.player2["esMano"] == True:
            v = 3
            while v > 0:
                self.player2["mano"].append(self.mazo.mazo[0])
                self.mazo.mazo.remove(self.mazo.mazo[0])
                self.player1["mano"].append(self.mazo.mazo[0])
                self.mazo.mazo.remove(self.mazo.mazo[0])
                v -= 1
        self.turno = 1

    # Bubble sort que ordena las cartas de mas grandes a mas chicas tomando figuras como más chicas
    def bubbleSort_envido(self, cards):
        aux = []
        # arr = []
        for i in range(len(cards)-1):
            for j in range(len(cards)-i-1):
                if cards[j]["numero"] < cards[j+1]["numero"] and cards[j+1]["envido"] == True or cards[j]["envido"] == False:
                    aux = cards[j]
                    cards[j] = cards[j+1]
                    cards[j+1] = aux
        return cards

    def envido(self, puntos):
        if self.canEnvido == True:
            cards1 = []
            cards2 = []
            envido1 = 0
            envido2 = 0
            # Si hay 3 del mismo palo
            if self.player1["mano"][0]["palo"] == self.player1["mano"][1]["palo"] and self.player1["mano"][0]["palo"] == self.player1["mano"][2]["palo"]:
                # Toma las 3
                cards1 = [self.player1["mano"][0],
                          self.player1["mano"][1], self.player1["mano"][2]]
                # Y les hace un sort para poder elegir las que más convienen
                cards1 = self.bubbleSort_envido(cards1)
                print("3 del mismo palo")
            else:  # Si no son las 3 iguales
                for card1 in self.player1["mano"]:
                    # Pasa por todas las combinaciones de cartas con un doble for
                    for card2 in self.player1["mano"]:
                        # Si tienen el mismo palo pero no es la misma carta
                        if card1["palo"] == card2["palo"] and card1["nombre"] != card2["nombre"]:
                            # Toma esas 2 cartas para el envido
                            cards1 = [card1, card2]
                            break                       # y termina ahí
            # Si hay 3 del mismo palo
            if self.player2["mano"][0]["palo"] == self.player2["mano"][1]["palo"] and self.player2["mano"][0]["palo"] == self.player2["mano"][2]["palo"]:
                # Toma las 3
                cards2 = [self.player2["mano"][0],
                          self.player2["mano"][1], self.player2["mano"][2]]
                # Y les hace un sort para poder elegir las que más convienen
                cards2 = self.bubbleSort_envido(cards2)
                print("3 del mismo palo")
            else:  # Si no son las 3 iguales
                for card1 in self.player2["mano"]:
                    # Pasa por todas las combinaciones de cartas con un doble for
                    for card2 in self.player2["mano"]:
                        # Si tienen el mismo palo pero no es la misma carta
                        if card1["palo"] == card2["palo"] and card1["nombre"] != card2["nombre"]:
                            # Toma esas 2 cartas para el envido
                            cards2 = [card1, card2]
                            break                        # y termina ahí
            # print("cards: ", cards1)
            # Se fija si puede acceder a cards[0] para ver si hay algo en cards
            try:
                # Si no son figuras se le suma su valor
                envido1 += cards1[0]["numero"] if cards1[0]["envido"] else 0
                envido1 += cards1[1]["numero"] if cards1[1]["envido"] else 0
                envido1 += 20  # Se le suma 20 del envido
            except IndexError:  # Si tira IndexError quiere decir que no encontró envido
                cards1 = [self.player1["mano"][0], self.player1["mano"]
                          [1], self.player1["mano"][2]]  # Le pasa las 3 cartas
                cards1 = self.bubbleSort_envido(cards1)  # Les hace un sort
                envido1 += cards1[0]["numero"]  # Agarra la mas grande
            # Se fija si puede acceder a cards[0] para ver si hay algo en cards
            try:
                # Si no son figuras se le suma su valor
                envido2 += cards2[0]["numero"] if cards2[0]["envido"] else 0
                envido2 += cards2[1]["numero"] if cards2[1]["envido"] else 0
                envido2 += 20  # Se le suma 20 del envido
            except IndexError:  # Si tira IndexError quiere decir que no encontró envido
                cards2 = [self.player2["mano"][0], self.player2["mano"]
                          [1], self.player2["mano"][2]]  # Le pasa las 3 cartas
                cards2 = self.bubbleSort_envido(cards2)  # Les hace un sort
                envido2 += cards2[0]["numero"]  # Agarra la mas grande

            if self.player1["esMano"] == True:
                if envido1 >= envido2:
                    envido2 = "Son buenas"
                    if puntos == "FALTA":
                        puntos = 30 - self.player2["puntos"]
                    self.player1["puntos"] += puntos
                else:
                    if puntos == "FALTA":
                        puntos = 30 - self.player1["puntos"]
                    self.player2["puntos"] += puntos
            else:
                if envido2 >= envido1:
                    envido1 = "Son buenas"
                    if puntos == "FALTA":
                        puntos = 30 - self.player1["puntos"]
                    self.player2["puntos"] += puntos
                else:
                    if puntos == "FALTA":
                        puntos = 30 - self.player2["puntos"]
                    self.player1["puntos"] += puntos
            self.canEnvido = False
            return(envido1, envido2)
        else:
            return("err")

    def ponerCarta(self, carta, player):
        self.added = not(self.added)
        if self.added == True:
            self.numRonda += 1
        self.mesa[self.numRonda - 1][player - 1] = carta
        if player == 1:
            self.player1["mano"].remove(carta)
        else:
            self.player2["mano"].remove(carta)
        self.rondas = []
        for cartas in self.mesa:
            try:
                if cartas[0]["valor"] > cartas[1]["valor"]:
                    self.rondas.append("player1")
                elif cartas[0]["valor"] < cartas[1]["valor"]:
                    self.rondas.append("player2")
                elif cartas[0]["valor"] == cartas[1]["valor"] and (cartas[0] and cartas[1]) != None:
                    self.rondas.append("p")
                self.canEnvido = False
                if self.numRonda > 1:
                    self.hasWon()
            except:
                pass

    def hasWon(self):
        p1p = 0
        p2p = 0
        for v in self.rondas:
            if v == "player1":
                p1p += 1
            elif v == "player2":
                p2p += 1
            elif v == "p":
                p1p += 1
                p2p += 1
        if p1p == 3 and p2p == 3:
            if self.player1["esMano"] == True:
                self.player1["puntos"] += self.puntosMano
            elif self.player2["esMano"] == True:
                self.player2["puntos"] += self.puntosMano
            self.finMano()
            # self.repartir()
            # self.startMano()
        elif p1p >= 2 and p1p > p2p:
            self.player1["puntos"] += self.puntosMano
            self.finMano()
            # self.repartir()
            # self.startMano()
        elif p2p >= 2 and p2p > p1p:
            self.player2["puntos"] += self.puntosMano
            self.finMano()
            # self.repartir()
            # self.startMano()

    def finMano(self):
        array = self.player1["mano"]
        array2 = self.player2["mano"]
        self.player1["mano"] = []
        self.player2["mano"] = []
        self.mazo.mazo.extend(array)
        self.mazo.mazo.extend(array2)
        self.player1["esMano"] = not self.player1["esMano"]
        self.player2["esMano"] = not self.player2["esMano"]
        self.won = True
