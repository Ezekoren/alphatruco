import truco
import random


class turno:
    def __init__(self):
        self.game = truco.Game()
        self.newMano()

    def newMano(self):
        self.game.mazo.mezclar()
        rand = random.randint(1, 40)
        print(rand)
        self.game.mazo.cortar(rand)
        self.game.startMano()
        self.packet = {
            "player1": self.game.player1,
            "player2": self.game.player2,
            "both": {
                "contador": [self.game.player1["puntos"], self.game.player2["puntos"]],
                "turno": self.game.numRonda,
                "mesa": self.game.mesa,
                "last": None
            },
            "options": ["t", "e", "re", "fe", "m"],
        }
        self.updatePacket()
        if self.game.player1["esMano"]:
            self.packet["both"]["juega"] = "player1"
            self.packet["both"]["proximoEnPoner"] = "player1"
            for carta in self.packet["player1"]["mano"]:
                self.packet["options"].append(carta["abreviatura"])
        else:
            self.packet["both"]["juega"] = "player2"
            self.packet["both"]["proximoEnPoner"] = "player2"
            for carta in self.packet["player2"]["mano"]:
                self.packet["options"].append(carta["abreviatura"])

        self.packet["both"]["last"] = None
        self.envidoPuntos = 0
        self.envidoNqPuntos = 0
        return(self.packet)

    def play(self, move):
        if move not in self.packet["options"]:
            return(self.packet)
        else:

            todelete = ["t", "rt", "vc"]
            if self.game.numRonda > 0:
                todelete.append("e")
                todelete.append("re")
                todelete.append("fe")
            for option in self.packet["options"]:
                for char in list(option):
                    if char.isdigit():
                        todelete.append(option)
                        break
            for v in todelete:
                try:
                    self.packet["options"].remove(v)
                except:
                    pass

            if self.packet["both"]["juega"] == "player1":
                self.packet["both"]["juega"] = "player2"
                if self.packet["player2"]["puedeCantar"] == True:
                    if self.game.puntosMano == 1:
                        self.packet["options"].append("t")
                    elif self.game.puntosMano == 2:
                        self.packet["options"].append("rt")
                    elif self.game.puntosMano == 3:
                        self.packet["options"].append("v4")
            else:
                self.packet["both"]["juega"] = "player1"
                if self.packet["player1"]["puedeCantar"] == True:
                    if self.game.puntosMano == 1:
                        self.packet["options"].append("t")
                    elif self.game.puntosMano == 2:
                        self.packet["options"].append("rt")
                    elif self.game.puntosMano == 3:
                        self.packet["options"].append("v4")

            if move == "e":
                self.packet["both"]["last"] = "e"
                if self.envidoNqPuntos == 0:
                    self.envidoNqPuntos = 1
                else:
                    self.envidoNqPuntos = self.envidoPuntos
                self.envidoPuntos += 2
                self.packet["options"] = ["q", "nq", "re", "fe"]
                if self.envidoPuntos == 2:
                    self.packet["options"].append("e")
                self.updatePacket()
                return(self.packet)

            elif move == "re":
                self.packet["both"]["last"] = "re"
                if self.envidoNqPuntos == 0:
                    self.envidoNqPuntos = 1
                else:
                    self.envidoNqPuntos = self.envidoPuntos
                self.envidoPuntos += 3
                self.packet["options"] = ["q", "nq", "fe"]
                self.updatePacket()
                return(self.packet)

            elif move == "fe":
                self.packet["both"]["last"] = "fe"
                if self.envidoNqPuntos == 0:
                    self.envidoNqPuntos = 1
                else:
                    self.envidoNqPuntos = self.envidoPuntos
                self.envidoPuntos = "FALTA"
                self.packet["options"] = ["q", "nq"]
                self.updatePacket()
                return(self.packet)

            elif move == "t":
                self.packet["both"]["last"] = "t"
                self.packet["options"] = ["q", "nq", "rt"]
                if self.game.canEnvido == True:
                    self.packet["options"].append("e")
                    self.packet["options"].append("re")
                    self.packet["options"].append("fe")
                self.updatePacket()
                return(self.packet)

            elif move == "rt":
                self.packet["both"]["last"] = "rt"
                self.game.puntosMano += 1
                self.packet["options"] = ["q", "nq", "vc"]
                self.updatePacket()
                return(self.packet)

            elif move == "vc":
                self.packet["both"]["last"] = "vc"
                self.game.puntosMano += 1
                self.packet["options"] = ["q", "nq"]
                self.updatePacket()
                return(self.packet)

            elif move == "q":
                if self.packet["both"]["last"] in ("t", "rt", "vc"):
                    self.game.puntosMano += 1
                    self.packet["options"] = ["m"]
                    if self.packet["both"]["juega"] == "player1":
                        self.game.player1["puedeCantar"] = True
                        self.game.player2["puedeCantar"] = False
                    else:
                        self.game.player2["puedeCantar"] = True
                        self.game.player1["puedeCantar"] = False
                    self.packet["both"]["juega"] = self.packet["both"]["proximoEnPoner"]
                    if self.packet["both"]["juega"] == "player1":
                        for carta in self.packet["player1"]["mano"]:
                            self.packet["options"].append(carta["abreviatura"])
                    else:
                        for carta in self.packet["player2"]["mano"]:
                            self.packet["options"].append(carta["abreviatura"])
                    self.updatePacket()
                    return(self.packet)

                elif self.packet["both"]["last"] in ("e", "re", "fe"):
                    self.packet["both"]["envido"] = self.game.envido(
                        self.envidoPuntos)
                    self.packet["options"] = ["t", "m"]
                    self.packet["both"]["juega"] = self.packet["both"]["proximoEnPoner"]
                    if self.packet["both"]["juega"] == "player1":
                        for carta in self.packet["player1"]["mano"]:
                            self.packet["options"].append(carta["abreviatura"])
                    else:
                        for carta in self.packet["player2"]["mano"]:
                            self.packet["options"].append(carta["abreviatura"])
                    self.updatePacket()
                    return(self.packet)

            elif move == "nq":
                if self.packet["both"]["last"] in ("e", "re", "fe"):
                    self.game.canEnvido = False
                    if self.packet["both"]["juega"] == "player1":
                        self.game.player1["puntos"] += self.envidoNqPuntos
                    else:
                        self.game.player2["puntos"] += self.envidoNqPuntos
                    self.packet["both"]["juega"] = self.packet["both"]["proximoEnPoner"]
                    self.packet["options"] = ["t", "m"]
                    self.packet["both"]["juega"] = self.packet["both"]["proximoEnPoner"]
                    if self.packet["both"]["juega"] == "player1":
                        for carta in self.packet["player1"]["mano"]:
                            self.packet["options"].append(carta["abreviatura"])
                    else:
                        for carta in self.packet["player2"]["mano"]:
                            self.packet["options"].append(carta["abreviatura"])
                    self.updatePacket()
                    return(self.packet)

                elif self.packet["both"]["last"] in ("t", "rt", "vc"):
                    if self.packet["both"]["proximoEnPoner"] == "player1":
                        self.game.player2["puntos"] += self.game.puntosMano
                    else:
                        self.game.player1["puntos"] += self.game.puntosMano
                    self.game.finMano()
                    self.newMano()
                    return(self.packet)

            elif move == "m":
                if self.packet["both"]["proximoEnPoner"] == "player1":
                    self.game.player2["puntos"] += self.game.puntosMano
                else:
                    self.game.player1["puntos"] += self.game.puntosMano
                self.game.finMano()
                self.newMano()
                return(self.packet)

            else:
                if self.packet["both"]["proximoEnPoner"] == "player1":
                    for carta in self.packet["player1"]["mano"]:
                        if move == carta["abreviatura"]:
                            self.game.ponerCarta(carta, 1)
                            if self.game.won:
                                self.newMano()
                                return(self.packet)
                    if len(self.game.rondas) == 0:
                        self.packet["both"]["proximoEnPoner"] = "player2"
                        self.packet["both"]["juega"] = "player2"
                    else:
                        try:
                            if self.game.rondas[self.game.numRonda - 1] == "player1":
                                self.packet["both"]["proximoEnPoner"] = "player1"
                                self.packet["both"]["juega"] = "player1"
                            elif self.game.rondas[self.game.numRonda - 1] == "player2":
                                self.packet["both"]["proximoEnPoner"] = "player2"
                                self.packet["both"]["juega"] = "player2"
                            elif self.game.rondas[self.game.numRonda - 1] == "p":
                                self.packet["both"]["proximoEnPoner"] = "player2"
                                self.packet["both"]["juega"] = "player2"
                        except:
                            self.packet["both"]["proximoEnPoner"] = "player2"
                            self.packet["both"]["juega"] = "player2"
                else:
                    for carta in self.packet["player2"]["mano"]:
                        if move == carta["abreviatura"]:
                            self.game.ponerCarta(carta, 2)
                            if self.game.won:
                                self.newMano()
                                return(self.packet)
                    if len(self.game.rondas) == 0:
                        self.packet["both"]["proximoEnPoner"] = "player1"
                        self.packet["both"]["juega"] = "player1"
                    else:
                        try:
                            if self.game.rondas[self.game.numRonda - 1] == "player1":
                                self.packet["both"]["proximoEnPoner"] = "player1"
                                self.packet["both"]["juega"] = "player1"
                            elif self.game.rondas[self.game.numRonda - 1] == "player2":
                                self.packet["both"]["proximoEnPoner"] = "player2"
                                self.packet["both"]["juega"] = "player2"
                            elif self.game.rondas[self.game.numRonda - 1] == "p":
                                self.packet["both"]["proximoEnPoner"] = "player1"
                                self.packet["both"]["juega"] = "player1"
                        except:
                            self.packet["both"]["proximoEnPoner"] = "player1"
                            self.packet["both"]["juega"] = "player1"
                if self.packet["both"]["juega"] == "player1":
                    for carta in self.packet["player1"]["mano"]:
                        self.packet["options"].append(carta["abreviatura"])
                else:
                    for carta in self.packet["player2"]["mano"]:
                        self.packet["options"].append(carta["abreviatura"])
                self.updatePacket()
                return(self.packet)

    def updatePacket(self):
        self.packet["player1"] = self.game.player1
        self.packet["player2"] = self.game.player2
        self.packet["both"]["contador"] = [
            self.game.player1["puntos"], self.game.player2["puntos"]]
        self.packet["both"]["turno"] = self.game.numRonda
        self.packet["both"]["mesa"] = self.game.mesa
