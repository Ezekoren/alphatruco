from flask import Flask, request
from flask_cors import CORS
import json
from truco import Game

app = Flask(__name__)
CORS(app)
game = Game()
player = 0

def manage_card(player, card):
    switch = {"player1": game.player1, "player2": game.player2}
    cartas_ronda = []
    if switch[player]["esTurno"]:
        cartas_ronda.append(switch[player]["mano"][int(card)])
        msg = "Pusiste el " + game.player1["mano"][int(card)]["nombre"]
    else:
        msg = "Lo siento, espera a tu turno"
    return cartas_ronda, msg

@app.route("/truco", methods=["POST"])
def handle_request():
    global player
    req = json.loads(request.data)
    comm = req["comm"]
    res = json.dumps({"response": "default"})
    if comm == "hola":
        game.repartir()
        res = json.dumps({"response": "se repartió"})

    elif comm == "getPlayer":
        if player <= 1:
            player += 1
            res = json.dumps({"response": player})
        else:
            res  = json.dumps({"response": -1})
        
    elif comm == "startGame":
        game.repartir()
        game.startMano()

    elif comm == "waitForPlayer":
        isPlayer2 = True if player == 2 else False
        res = json.dumps({"response": isPlayer2})

    elif comm == "drawCard":
        cartas_ronda, msg = manage_card(req["player"], req["carta"])
        res = json.dumps({"response": msg})
        
    else:
        res = json.dumps({"response": "no entendi el comando"})
    
    return res

app.run()